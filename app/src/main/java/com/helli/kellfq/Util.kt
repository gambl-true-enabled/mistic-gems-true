package com.helli.kellfq

import android.content.Context
import android.webkit.JavascriptInterface

private const val GEM_TABLE = "com.GEM.table.123"
private const val GEM_ARGS = "com.GEM.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(GEM_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(GEM_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(GEM_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(GEM_ARGS, null)
}
