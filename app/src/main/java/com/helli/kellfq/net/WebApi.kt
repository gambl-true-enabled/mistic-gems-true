package com.helli.kellfq.net

import com.helli.kellfq.net.RawData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface WebApi {
    @GET("check/")
    suspend fun check(@Header("appsflyer_id") uid: String): Response<RawData>
}